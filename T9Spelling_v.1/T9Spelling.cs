﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace T9Spelling_v._1
{
    public class T9Spelling
    {
        static Dictionary<String, String> t9Dictionary;

        static void Main(string[] args)
        {
            //заносим в масив значения букв
            fillDictionary();
            // для вывода результата в файл
            FileOutput();
            //для вывода результата в консоль
            ConsoleOutput();

            void ConsoleOutput()
            {
                //поток для чтения смс из файла
                StreamReader reader = new StreamReader(@"..\..\C-small-practice.in");
                //получаем колличество смс
                int cases = Int32.Parse(reader.ReadLine());
               
                for (int i = 1; i <= cases; i++)
                {
                    Console.Out.Write("Case #" + i + ": ");

                    String t9, t9last = "";

                    String text = reader.ReadLine();

                    for (int j = 0; j < text.Length; j++)
                    {
                        t9 = t9Dictionary[text.ElementAt(j).ToString()];
                        // проверяем находятся ли буквы на одной кнопке если да ставим пробел
                        if (j > 0 && (t9[0]) == t9last[0])
                        {
                            Console.Out.Write(" ");
                        }
                        Console.Out.Write(t9);
                        t9last = t9;
                    }
                    Console.Out.WriteLine();
                }
                Console.ReadLine();
                // закрываем потоки
                reader.Close();
            }


            void FileOutput()
            {
                //поток для чтения смс из файла
                StreamReader reader = new StreamReader(@"..\..\C-small-practice.in");
                //поток для записи переведенных смс в файл
                StreamWriter writer = new StreamWriter(@"..\..\C-small-practice.in-encode.in");
                int cases = Int32.Parse(reader.ReadLine());
             
                for (int i = 1; i <= cases; i++)
                {
                    writer.Write("Case #" + i + ": ");

                    String t9, t9last = "";

                    String text = reader.ReadLine();

                    for (int j = 0; j < text.Length; j++)
                    {
                        t9 = t9Dictionary[text.ElementAt(j).ToString()];
                        // проверяем находятся ли буквы на одной кнопке если да ставим пробел
                        if (j > 0 && (t9[0]) == t9last[0])
                        {
                            writer.Write(" ");
                        }
                        writer.Write(t9);
                        t9last = t9;
                    }
                    writer.WriteLine();
                }
                // закрываем потоки
                reader.Close();
                writer.Close();
            }


            //заносим в масив значения букв
            void fillDictionary()
            {
                t9Dictionary = new Dictionary<String, String>
                {
                    ["a"] = "2",
                    ["b"] = "22",
                    ["c"] = "222",

                    ["d"] = "3",
                    ["e"] = "33",
                    ["f"] = "333",

                    ["g"] = "4",
                    ["h"] = "44",
                    ["i"] = "444",

                    ["j"] = "5",
                    ["k"] = "55",
                    ["l"] = "555",

                    ["m"] = "6",
                    ["n"] = "66",
                    ["o"] = "666",

                    ["p"] = "7",
                    ["q"] = "77",
                    ["r"] = "777",
                    ["s"] = "7777",

                    ["t"] = "8",
                    ["u"] = "88",
                    ["v"] = "888",

                    ["w"] = "9",
                    ["x"] = "99",
                    ["y"] = "999",
                    ["z"] = "9999",

                    [" "] = "0"
                };
            }
        }
    }
}